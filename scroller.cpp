#include "scroller.h"
#include "raptorsliderviewgroup.h"

class Scroller::Private {
    public:
        Private();
        RaptorSliderViewGroup *sliderView;
        RaptorSlide::Direction mode;
        bool hovered;
};

Scroller::Private::Private()
{
    mode = RaptorSlide::Grid;
    sliderView = 0;
//     sliderView = new RaptorSlidersliderViewGroup();
//     sliderView->setMode(mode);
}

Scroller::Scroller(QGraphicsItem *parent = 0)
    : QGraphicsItem(parent)
{
    d = new Private;
//     d->

}

Scroller::~Scroller()
{
    delete d;
}

void Scroller::setMode(RaptorSlide::Direction mode)
{
    d->mode = mode;
    d->sliderView->setMode(mode);
}

void Scroller::addItem(RaptorClaw *item)
{
    d->sliderView->addItem(item);
}

void Scroller::clearItems()
{
    d->sliderView->clearItems();
}

QRectF Scroller::boundingRect()
{
    return d->sliderView->boundingRect();
}

void Scroller::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

#include "scroller.moc"