#include "raptorclaw.h"
#include <QPainter>
#include <KIcon>
#include <kicontheme.h>
#include "raptorclaw.moc"

class RaptorClaw::Private
{
    public:
        Private(){}
        ~Private(){}
        int height;
        int width;
        Plasma::Svg * m_theme;
        QString text;
        QString comment;
        QString tooltip;
        QString icon;
        int Rating;
        MouseState state;
        ThemeNames ids;
        QSize size;
        QSize elementSize;
        QPixmap iconPixmap;
        float opacity;
        //Grid
        int ncol;
        int nrow;
};


RaptorClaw::RaptorClaw(QGraphicsItem *parent)
    : QGraphicsItem(parent)
{
    d = new Private;
    //TODO
    setAcceptsHoverEvents(true);
        //read from config 
    d->height = 330;
    d->width = 340;
    d->m_theme = new Plasma::Svg( "menu/raptorslide",this);
    d->m_theme->resize(d->width, d->height);
    //FIXME
    //Do not hardcode Get from config/xml
    d->text = "RaptorItem";
    d->ids[REGULAR] = "itemNormal";
    d->ids[OVER] = "itemOver";
    d->ids[PRESSED] = "itemOver";
    d->state = REGULAR;
    d->size = d->m_theme->size();
    d->elementSize = d->m_theme->elementSize("itemNormal");
    d->iconPixmap = QPixmap();
    d->opacity = 1.0f;
    //FIXME: need methods for these
    d->nrow=4;
    d->ncol=4;
}

RaptorClaw::~RaptorClaw()
{
    delete d->m_theme;
    delete d;
}

void RaptorClaw::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);
    painter->setOpacity(d->opacity);
    painter->setRenderHint(QPainter::SmoothPixmapTransform);
    d->m_theme->resize(d->width,d->height);
    QRectF source(x(), y(), d->elementSize.width() , d->elementSize.height());
    d->m_theme->paint(painter, (int)x(),(int)y(), d->ids[d->state]);
    if (!d->iconPixmap.isNull()) {
        painter->drawPixmap(int(x()+((int)source.width()-d->iconPixmap.width())/2),int(y()+ 
                ((int)source.height()-d->iconPixmap.height())/2),d->iconPixmap.height(),d->iconPixmap.width(),d->iconPixmap);
    } else {
    kDebug()<<"NUll Icon Pixmap"<<endl;
    }

    //painter->drawText(source,Qt::AlignCenter,d->text); 
}

void RaptorClaw::setOpacity(float op)
{
    d->opacity = op;
}
QString RaptorClaw::loadSvg(MouseState state)
{
    return d->ids[state];
}

QSize RaptorClaw::sizeHint() const
{
    return QSize(d->height,d->width);
}

QSize RaptorClaw::minimumSize() const
{
    return QSize(d->height,d->width);
}

QRect RaptorClaw::geometry() const
{
    return QRect((int)x(),(int)y(),d->height,d->width);
}

QSize RaptorClaw::maximumSize() const
{
    return QSize(d->height,d->width);
}

QRectF RaptorClaw::boundingRect() const
{
    return QRectF (x(),y(),d->elementSize.width(),d->elementSize.height());
}

//Events
void RaptorClaw::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    d->state = OVER;
    update();
}

void RaptorClaw::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    d->state = OVER;
    update();
}

void RaptorClaw::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    d->state = REGULAR;
    update();
}

QString RaptorClaw::name()
{
    return d->text;
}

void RaptorClaw::setName(const QString& name)
{
    d->text = name;
}

void RaptorClaw::setIcon(QPixmap icon)
{
    d->iconPixmap = QPixmap(icon);
}
