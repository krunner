#include "raptorsliderviewgroup.h"
#include <QTimer>
#include <plasma/phase.h>

class RaptorSliderViewGroup::Private
{
    public:
        Private(){}
        ~Private(){}
        stackHash slideLayer;
        int slideCount;
        RaptorSlide *currentLayer;
        int currentViewId;
        QTimer *slideTimer;
        RaptorSlide::Direction mode;
};

RaptorSliderViewGroup::RaptorSliderViewGroup(QGraphicsScene *scene)
{
    d->currentLayer = new RaptorSlide();
    d->mode = RaptorSlide::Grid; // Default is grid
    d->currentLayer->setDirection(d->mode);
    d->currentLayer->move(10.0, 0.0);
    d->slideCount = 0;
    d->slideLayer[0] = d->currentLayer;
    canvas = scene;
    d->currentViewId = 0;
    d->slideTimer = new QTimer(this);
}

RaptorSliderViewGroup::~RaptorSliderViewGroup()
{
    delete d;
}

void RaptorSliderViewGroup::setMode(RaptorSlide::Direction m)
{
    d->mode = m;
}

bool RaptorSliderViewGroup::addItem(RaptorClaw *item)
{
    if (!d->currentLayer->isFull()) {
        d->currentLayer->addItem((RaptorClaw*)item);
        canvas->addItem(item);
        item->hide();
    } else {
        d->currentLayer = new RaptorSlide();
        d->currentLayer->setDirection(d->mode);
        d->slideLayer[++d->slideCount] = d->currentLayer;
        d->currentLayer->addItem(item);
        canvas->addItem(item);
        item->hide();
    }

}

void RaptorSliderViewGroup::clearItems()
{
    while (d->slideCount == 0) {
        delete d->slideLayer[d->slideCount];
        d->slideCount--;
    }
}

QGraphicsItem* RaptorSliderViewGroup::find(const QString& key)
{
}

int RaptorSliderViewGroup::slideCount()
{
    return d->slideCount;
}

void RaptorSliderViewGroup::view(int id)
{
    // we want to hide quick for now.. 
    // TODO : add phase animation integration.here
    for (int i = 0; i < d->slideLayer.count(); i++) {
        // Plasma::Phase().animateItem(d->slideLayer[i],Plasma::Phase::Disappear);
        // Plasma::Phase().moveItem(d->slideLayer[i],Plasma::Phase::SlideOut,QPoint(400,400));
        d->slideLayer[i]->hide();
        }
        if (d->slideLayer[id]) {
            d->slideLayer[id]->show();
    }
    d->currentViewId= id;
}
//protected

void RaptorSliderViewGroup::swapNext()
{
    if ((d->currentViewId  ) < d->slideCount)
    {
            d->currentViewId = d->currentViewId + 1;
            this->view(d->currentViewId);
    }
    qDebug("Now Swapping");
}

void RaptorSliderViewGroup::swapPrevious()
{
    if ( d->currentViewId -1 >= 0) {
        this->view(--d->currentViewId);
    }
}

void RaptorSliderViewGroup::move(float x , float y)
{
    for (int i = 0; i < d->slideLayer.count(); i++) {
        d->slideLayer[i]->move(x,y);
    }
}

float RaptorSliderViewGroup::height()
{
   return  d->currentLayer->height();
}

#include "raptorsliderviewgroup.moc"
