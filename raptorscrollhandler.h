//GPL2 siraj@kde.org


#ifndef RAPTOR_SCROLL_HANDLER_H
#define RAPTOR_SCROLL_HANDLER_H

#include <QGraphicsItem>
#include <QtCore/QObject>
#include <QtGui/QGraphicsTextItem>
#include <QtGui/QLayoutItem>
//plasma
#include <plasma/svg.h>
#include <plasma/theme.h>
#include <plasma/datacontainer.h>


class RaptorScrollHandler : public QObject,
                            public QGraphicsItem,
                            public QLayoutItem
{
    Q_OBJECT

    public:
        typedef enum OwnState {REGULAR, Hovered, PRESSED};
        typedef QHash <OwnState, QString> ElementHash;
        typedef enum ArrowState {UP=0, DOWN, LEFT, RIGHT};

        RaptorScrollHandler(QGraphicsItem * parent = 0);

        virtual ~RaptorScrollHandler();

        QRectF boundingRect() const;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
        QSize sizeHint() const;
        QSize minimumSize() const {} //TODO Implement me
        QSize maximumSize() const {} //TODO Implement me
        void setArrowState(RaptorScrollHandler::ArrowState state);
        Qt::Orientations expandingDirections() const{} //TODO Implement me
        void setExpandingDirections(Qt::Orientations ori);
        void setGeometry(const QRect &geometry);
        QRect geometry() const;
        bool isEmpty() const { return false; }

    public slots:

     signals:
        void activated();
        void deactivated();

    protected:
        virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
        virtual void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
        virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
        virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);

    private:
        QString getElementNameForState(OwnState state);
        class Private;
        Private *d;
};

#endif
