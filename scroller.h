#ifndef SCROLLER_H
#define SCROLLER_H

#include <QGraphicsItemGroup>
// #include <QGraphicsItem>
// #include <QGraphicsScene>
// #include <QHash>
#include "raptorslide.h"
// #include "raptorclaw.h"
// #include "raptorscrollhandler.h"

class QRectF;

/**
* This class is meant to easily handle a complete
* "scroller" view, with slides, items and scrollhandlers.
*/

class Scroller : public QObject,
                 public QGraphicsItem
{
    Q_OBJECT

    public:
        Scroller(QGraphicsItem *parent);
        ~Scroller();

        void setMode(RaptorSlide::Direction mode);
        void addItem(RaptorClaw *item);
        void clearItems();
        virtual QRectF boundingRect();
        virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
//         typedef QHash <int,RaptorSlide*> stackHash;

//         RaptorSlidersliderViewGroup(QGraphicsScene *scene);
//         virtual ~RaptorSlidersliderViewGroup();
//         QGraphicsScene* getDefaultScene() { return canvas; }
//         bool addItem(RaptorClaw*);
//         bool deleteItem(QGraphicsItem*);
//         QGraphicsItem* find(const QString&);
//         void setMode(RaptorSlide::Direction);
//         int slideCount();
//         void sliderView(int id);
//         void move(float x , float y);
//         float height();

    public slots:
//         void swapNext();
//         void swapPrevious();
            //TODO
            //swap by Name :
            //Swap by Integer ID

 //protected:
//        void addScrolls();

  private:
        class Private;
        Private *d;
};

#endif