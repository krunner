//GPL2 cod : siraj@kde.org

#ifndef RAPTOR_SLIDER_VIEW_GROUP_H
#define RAPTOR_SLIDER_VIEW_GROUP_H

#include <QGraphicsItemGroup>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QHash>
#include "raptorslide.h"
#include "raptorclaw.h"
#include "raptorscrollhandler.h"


class RaptorSliderViewGroup : public QObject,
                              public QGraphicsItemGroup
{
    Q_OBJECT

    public:
        typedef QHash <int,RaptorSlide*> stackHash;

        RaptorSliderViewGroup(QGraphicsScene *scene);
        virtual ~RaptorSliderViewGroup();
        QGraphicsScene* getDefaultScene() { return canvas; }
        bool addItem(RaptorClaw *);
        void clearItems();
        QGraphicsItem* find(const QString&);
        void setMode(RaptorSlide::Direction);
        int slideCount();
        void view(int id);
        void move(float x , float y);
        float height();

    public slots:
        void swapNext();
        void swapPrevious();
            //TODO
            //swap by Name :
            //Swap by Integer ID

 //protected:
//        void addScrolls();

  private:
        class Private;
        Private *d;
        QGraphicsScene *canvas;
};

#endif
