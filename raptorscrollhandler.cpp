#include "raptorscrollhandler.h"
#include <QPainter>
#include "raptorscrollhandler.moc"

class RaptorScrollHandler::Private
{
    public:
        Private(){}
        ~Private(){}
        int height;
        int width;
        Plasma::Svg *m_theme;
        QString comment;
        QString tooltip;
        OwnState state;
        ElementHash elementsNames;
        QSize size;
        QSize elementSize;
        QRect geometry;
        Qt::Orientations orientation;
        ArrowState arrow;
};


RaptorScrollHandler::RaptorScrollHandler(QGraphicsItem * parent)
    : QGraphicsItem(parent)
{
    d = new Private;
    //TODO
    setAcceptsHoverEvents(true);
    //read from config
    d->height = 240;
    d->width = 340;
    d->m_theme = new Plasma::Svg("menu/raptorslide",this);
    d->m_theme->resize(d->height, d->width);
    //TODO don't do this!
    setExpandingDirections(Qt::Vertical);
    d->state = REGULAR;
    d->size = d->m_theme->size();
    //d->size  = QSize(d->size.width()/10,d->size.height()/10);
    d->elementSize = d->m_theme->elementSize("arrowUp");
    d->arrow = UP;
    d->orientation = Qt::Vertical;
}

void RaptorScrollHandler::setArrowState(RaptorScrollHandler::ArrowState state)
{
    if (state == UP) {
         d->elementsNames[REGULAR] = "arrowUp";
         d->elementsNames[Hovered] = "arrowUp";
         d->elementsNames[PRESSED] = "arrowUp";
    } else if (state == DOWN) {
         d->elementsNames[REGULAR] = "arrowDown";
         d->elementsNames[Hovered] = "arrowDown";
         d->elementsNames[PRESSED] = "arrowDown";
    } else if (state == LEFT) {
         d->elementsNames[REGULAR] = "arrowLeft";
         d->elementsNames[Hovered] = "arrowLeft";
         d->elementsNames[PRESSED] = "arrowLeft";
         d->elementSize = d->m_theme->elementSize("arrowLeft");
    } else if (state == RIGHT) {
         d->elementsNames[REGULAR] = "arrowRight";
         d->elementsNames[Hovered] = "arrowRight";
         d->elementsNames[PRESSED] = "arrowRight";
         d->elementSize = d->m_theme->elementSize("arrowRight");
    }

    d->arrow = state;
}


RaptorScrollHandler::~RaptorScrollHandler()
{
//     delete d->m_theme;
    delete d;
}

void RaptorScrollHandler::setExpandingDirections(Qt::Orientations ori)
{
    Q_UNUSED(ori);
 /*
    d->orientation = ori;
    if (d->orientation == Qt::Vertical) {
        d->elementsNames[REGULAR] = "arrowUp";
        d->elementsNames[Hovered] = "arrowUp";
        d->elementsNames[PRESSED] = "arrowUp";
    } else {
        d->elementsNames[REGULAR] = "arrowBot";
        d->elementsNames[Hovered] = "arrowBot";
        d->elementsNames[PRESSED] = "arrowDown";

    }
    */
}

void RaptorScrollHandler::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    d->m_theme->resize(d->height, d->width);
    painter->setRenderHint(QPainter::SmoothPixmapTransform);
    QRectF source = boundingRect();//(x(), y(), d->width , d->height );
    d->m_theme->paint(painter, x(), y(), d->elementsNames[d->state]);
    //painter->drawRect(boundingRect());
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QString RaptorScrollHandler::getElementNameForState(OwnState state)
{
    return d->elementsNames[state];
}

QSize RaptorScrollHandler::sizeHint() const
{
    return QSize(d->width, d->height);
}

QRectF RaptorScrollHandler::boundingRect() const
{
    //kDebug() << QRectF (x(),y(),d->elementSize.width(),d->elementSize.height());
    return QRectF (x(), y(), d->elementSize.width(), d->elementSize.height());
}

void RaptorScrollHandler::setGeometry(const QRect &geometry)
{
    d->geometry = geometry;
}

QRect RaptorScrollHandler::geometry() const
{
    return d->geometry;
}


/*
QRect RaptorScrollHandler::geometry () const
{
    return QRect (x(),y(),d->width,d->height);
}

*/

//Events

void RaptorScrollHandler::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    d->state = Hovered;
    emit activated();
    update();
}

void RaptorScrollHandler::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    d->m_theme->resize(d->width, d->height);
    d->state = Hovered;
    update();
}

void RaptorScrollHandler::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    d->state = REGULAR;
    emit deactivated();
    update();
}

void RaptorScrollHandler::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    d->state = Hovered;
    emit activated();
    update();
}
