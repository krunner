#include "raptorslide.h"
#include <KDebug>
#include <QTimer>

class RaptorSlide::Private
{
  public:
    Private(){}
    ~Private(){}
//        ItemList  m_itemList; //Hash of ponters indexed with integer key
        int m_index;//integer index for the slide
        int m_count;//current itemcount
        int m_max; // Max number of this slide. 
        QString m_name;//name of the slide
        float m_slideLength;
        bool m_visible;
        float x;
        float y;
        int width;
        int height;
        RaptorSlide::Direction m_SlideLayout;
        float opacity;
        QTimer * hideTimer;
        int ncol;
        int nrow;
        typedef QHash<int,QPoint> GridHash;
        GridHash ghash;
};

RaptorSlide::RaptorSlide(QGraphicsItem *parent)
    : QGraphicsItemGroup(parent)
{
    d = new Private;

    /*default states*/
    d->m_name = "";
    d->m_count =0;
    d->m_max = 4;
    d->m_slideLength = 0.0;
    d->m_SlideLayout  = Verticle;
    d->m_visible=false;
    d->x = 0.0;
    d->y = 0.0;
    d->opacity = 1.0;
    d->hideTimer = new QTimer(this);
    connect(d->hideTimer,SIGNAL(timeout()),this,SLOT(animate()));
    d->hideTimer->stop();
    d->nrow= 2;
    d->ncol=2;
}

RaptorSlide::~RaptorSlide()
{
    delete d;
}

/*
QMap<RaptorClaw*,RaptorSlide*>& RaptorSlide::groupMap()
{
        static GroupMap *group = 0; 
    if ( !group ) 
    {
       group = new GroupMap;
       return *group;
    }
}
*/

QString RaptorSlide::name()
{
    return d->m_name;
}

void RaptorSlide::setName(const QString &name)
{
    d->m_name = name;
}

bool RaptorSlide::addItem(RaptorClaw *item)
{
    if (item == NULL) {
        kDebug() << "Null Item not added" << endl;
        return false;
    }
    items.prepend(item);
    d->m_count++;
    doLayoutShift();
    return true;
}

void RaptorSlide::setDirection(RaptorSlide::Direction layout)
{
    d->m_SlideLayout = layout;
    doLayoutShift();
}

RaptorSlide::Direction RaptorSlide::direction()
{
    return d->m_SlideLayout;
}

void RaptorSlide::doLayoutShift()
{
    float len = (direction() == Horizontal)?this->d->x:this->d->y;

    if (direction() == Grid) {
        //Calcaulate Gird values.
        int itemHeight=0;
        int itemWidth=0;
        int totalHeight = itemHeight;
        int totalWidth = itemWidth;

        for (int i = 0 ; i < items.size();i++) {
            itemWidth = items.at(i)->boundingRect().width();
            itemHeight = items.at(i)->boundingRect().height();
            totalWidth +=itemWidth;
            totalHeight +=itemHeight;
        }

        int rowcount = 0;
        int x = 0;
        int y = 0;

        kDebug() << "item height , width" << totalWidth << totalHeight;

        for (int i =-0; i < items.size(); i++ ) {
            if (rowcount == 2) {
                x=0;
                y+=itemHeight/2;
            }
            d->ghash[i] = QPoint(x,y);
            items.at(i)->setPos(d->ghash[i]);
            x+=itemWidth/2;
            rowcount++;
        }

        return;
    }

    for (int i = 0; i < items.size(); ++i) {
       if (direction() == Horizontal) {
           items.at(i)->setPos(len/2,items.at(0)->y());
           len += items.at(i)->boundingRect().width();
       } else {
           //item at zero to avoice starecase effect
           items.at(i)->setPos(items.at(0)->x(),len/2);
           len += items.at(i)->boundingRect().height();
       }
    }
     d->height = len;
}

bool RaptorSlide::deleteItem(RaptorClaw *item)
{
    removeFromGroup(item);
    delete item;
    item = 0;
    return true;
}

RaptorClaw* RaptorSlide::find(const QString &key)
{
}

void RaptorSlide::show()
{
    for (int i = 0; i < items.size(); ++i) 
     {
         kDebug() << "Showing:"<< items.at(i)->name();
         items.at(i)->show();
         items.at(i)->setOpacity(1.0);
         items.at(i)->update();
     }

     d->m_visible = true;
}

void RaptorSlide::move(float x, float y , float z)
{
    d->x = x;
    d->y = y;

    if (items.count() >= 1) {
        items.at(0)->setPos(x,y);
    }

    doLayoutShift();
}

bool RaptorSlide::isFull()
{
    if (d->m_max == d->m_count) {
        return true;
    }
    return false;
}

void RaptorSlide::hide()
{
//    d->hideTimer->start(100);
    for (int i = 0; i < items.size(); ++i) 
    {
        items.at(i)->hide();
    }
    this->d->m_visible = false;
}

bool RaptorSlide::isVisible()
{
    return d->m_visible;
}

int RaptorSlide::height()
{
    return d->height;
}

void RaptorSlide::animate()
{
   d->opacity = 1.0;
}

#include "raptorslide.moc"
