//GPL2 cod : siraj@kde.org

#ifndef RAPTOR_SLIDE_H
#define RAPTOR_SLIDE_H

#include <QGraphicsItemGroup>
#include <QGraphicsItem>
#include <QListIterator> 
#include <QHash>
#include "raptorclaw.h"

/**
 * This takes keeps a pre-defined number of elements
 * and it there per view
 */

class RaptorSlide : public QObject,
                    public QGraphicsItemGroup
{
    Q_OBJECT

    public:
        //typedef QMap <int,QGraphicsItem*> ItemList;
        typedef enum {Verticle,Horizontal,Grid} Direction;
        typedef QList<RaptorClaw*> CList;
        typedef QListIterator<RaptorClaw> CListIter;

        RaptorSlide(QGraphicsItem * parent = 0);
        virtual ~RaptorSlide();
        bool addItem(RaptorClaw *);
        bool deleteItem(RaptorClaw *);
        RaptorClaw* find(const QString &);
        bool isFull();
        void setItemLimit(int);
        int itemLimit();
        void show();
        void hide();
        void setDirection(Direction d);
        Direction direction();
        QString name();
        void setName(const QString & name);
        bool isVisible();
        void move(float x, float y, float z = 0);
        int height();

    public slots:
        void animate();

    signals:
        void hideComplete();

    protected:
        void doLayoutShift();

    private:
        class Private;
        Private *d;
        CList items;

       // typedef QMap<RaptorClaw*,RaptorSlide*> GroupMap;
       // static GroupMap& groupMap();
       // GroupMap *  map;
};

#endif
